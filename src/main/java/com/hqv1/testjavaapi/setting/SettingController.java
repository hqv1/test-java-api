package com.hqv1.testjavaapi.setting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SettingController {

    @Autowired
    private AppProperties appProperties;

    @GetMapping("/settings/name")
    public String getApplicationName() {
        return appProperties.getName();
    }
}


