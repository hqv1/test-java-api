package com.hqv1.testjavaapi.setting;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

//https://www.mkyong.com/spring-boot/spring-boot-configurationproperties-example/
@Component
@ConfigurationProperties("app")
public class AppProperties {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
