package com.hqv1.testjavaapi.util;

import com.hqv1.testjavaapi.author.AuthorEntity;

public interface AuthorValidator {
    boolean shouldValidate(AuthorEntity author);
    String validate(AuthorEntity author);
}

