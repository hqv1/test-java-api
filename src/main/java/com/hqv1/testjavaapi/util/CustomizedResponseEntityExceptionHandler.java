package com.hqv1.testjavaapi.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LogManager.getLogger(CustomizedResponseEntityExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ExceptionResponse response =
                new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));

        logger.error("Internal server error from {}", request, ex);
        return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleNotFoundExceptions(Exception ex, WebRequest request) {
        ExceptionResponse response =
                new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));

        logger.info("The requested resource was not found: {}", request);
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DomainValidationException.class)
    public final ResponseEntity<Object> handleValidationExceptions(Exception ex, WebRequest request) {
        ExceptionResponse response =
                new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));

        logger.info("The request has validation errors {}", request, ex);
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    // Provides more detail validation error.
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ExceptionResponse response =
                new ExceptionResponse(new Date(), "Validation Failed", ex.getBindingResult().toString());

        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }
}
