package com.hqv1.testjavaapi.util;

import com.hqv1.testjavaapi.author.AuthorEntity;
import com.hqv1.testjavaapi.author.InMemoryAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SocialSecurityUniqueAuthorValidator implements AuthorValidator {

    @Autowired
    private InMemoryAuthorRepository authorRepository;

    public boolean shouldValidate(AuthorEntity author) {
        return true;
    }

    public String validate(AuthorEntity author) {
        List<AuthorEntity> authors = authorRepository.getAll();

        for(AuthorEntity a:authors) {
            if(a.getSocialSecurityNumber().equals(author.getSocialSecurityNumber())) {
                return "Social security number exist for another author";
            }
        }
        return null;
    }
}
