package com.hqv1.testjavaapi.author;

import com.fasterxml.jackson.annotation.JsonFilter;

//@JsonFilter("SomeAuthorFilter")
public class AuthorToGetModel {

    private int authorId;
    private String firstName;
    private String lastName;

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
