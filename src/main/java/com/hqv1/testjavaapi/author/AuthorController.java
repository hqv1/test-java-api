package com.hqv1.testjavaapi.author;

import com.hqv1.testjavaapi.util.NotFoundException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;

@RestController
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorService authorService;

    @GetMapping("/authors")
    public List<AuthorToGetModel> getAllAuthors() {

        List<AuthorEntity> authors = authorRepository.getAll();

        // todo: Create an interface (DomainMapper) and class to store mapping logic.
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<AuthorToGetModel>>() {}.getType();
        List<AuthorToGetModel> models = modelMapper.map(authors, listType);

        return models;
    }

    @GetMapping("/authors/{id}")
    public AuthorToGetModel getAuthor(@PathVariable int id) {
        AuthorEntity author = authorRepository.get(id);
        if(author == null) {
            throw new NotFoundException("Author id " + id);
        }

        // todo: Create an interface (DomainMapper) and class to store mapping logic.
        ModelMapper modelMapper = new ModelMapper();
        AuthorToGetModel model = modelMapper.map(author, AuthorToGetModel.class);
        return model;

        // Use if we allow filtering (e.g. /authors/4?fields=authorId,firstName
        /*SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("authorId");
        FilterProvider filters = new SimpleFilterProvider()
                .addFilter("SomeAuthorFilter", filter);
        MappingJacksonValue mapping = new MappingJacksonValue(model);
        mapping.setFilters(filters);
        return mapping;*/

    }

    @PostMapping("/authors")
    public ResponseEntity<Object> createAuthor(@Valid @RequestBody AuthorToPostModel model) {

        // todo: Create an interface (DomainMapper) and class to store mapping logic.
        ModelMapper modelMapper = new ModelMapper();
        AuthorEntity author = modelMapper.map(model, AuthorEntity.class);

        AuthorEntity savedAuthor = authorService.createAuthor(author);
        URI location = ServletUriComponentsBuilder.
                fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedAuthor.getAuthorId()).toUri();
        return ResponseEntity.created(location).build();
    }
}
