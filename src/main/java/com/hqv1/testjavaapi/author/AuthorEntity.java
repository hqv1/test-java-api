package com.hqv1.testjavaapi.author;

public class AuthorEntity {
    private int authorId;
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    // For model mapper
    protected AuthorEntity() {
    }

    public AuthorEntity(int authorId, String firstName, String lastName, String socialSecurityNumber) {
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
}
