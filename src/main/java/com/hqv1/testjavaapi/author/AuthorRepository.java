package com.hqv1.testjavaapi.author;

import java.util.List;

public interface AuthorRepository {
    List<AuthorEntity> getAll();

    AuthorEntity get(int id);

    AuthorEntity save(AuthorEntity author);
}
