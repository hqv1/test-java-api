package com.hqv1.testjavaapi.author;

import com.hqv1.testjavaapi.util.AuthorValidator;
import com.hqv1.testjavaapi.util.DomainValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorValidator authorValidator;


    public AuthorEntity createAuthor(AuthorEntity author) {

        //todo: add validators
        String validationResult = authorValidator.validate(author);
        if(validationResult != null) {
            throw new DomainValidationException(validationResult);
        }

        //todo: add enrichers

        AuthorEntity savedAuthor = authorRepository.save(author);
        return savedAuthor;
    }
}
