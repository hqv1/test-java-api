package com.hqv1.testjavaapi.author;

import javax.validation.constraints.Size;

public class AuthorToPostModel {

    @Size(min=2)
    private String firstName;

    @Size(min=2)
    private String lastName;
    private String socialSecurityNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

}
