package com.hqv1.testjavaapi.author;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InMemoryAuthorRepository implements AuthorRepository {

    private static List<AuthorEntity> authors = new ArrayList<>();
    private static int authorsCount = 3;

    public InMemoryAuthorRepository() {
        authors.add(new AuthorEntity(1, "Carmine", "Abate", "123456789"));
        authors.add(new AuthorEntity(2, "James", "Blish", "012345678"));
        authors.add(new AuthorEntity(3, "Meg", "Cabot", "234567890"));
    }

    @Override
    public List<AuthorEntity> getAll() {
        return authors;
    }

    @Override
    public AuthorEntity get(int id) {
        for(AuthorEntity author:authors) {
            if(author.getAuthorId() == id) {
                return author;
            }
        }
        return null;
    }

    @Override
    public AuthorEntity save(AuthorEntity author){
        author.setAuthorId(++authorsCount);
        authors.add(author);
        return author;
    }
}
