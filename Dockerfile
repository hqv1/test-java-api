FROM openjdk:8-jdk-alpine
LABEL maintainer="v.hoang@outlook.com"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/test-java-api-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} test-java-api.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/test-java-api.jar"]